function zodiac(date = prompt("Введите дату рождения (dd.mm.yyyy)")) {
    date = date.split('.');
    let temp = date[0];
    date[0] = date[1];
    date[1] = temp;
    date = new Date(date.join('/'));
    let years = Math.floor((new Date().getTime() - date.getTime())/(1000*60*60*24*365.242196));
    let dateZodiac = date.getMonth();
    let dayZodiac = date.getDay();
    let zodSigns = ["Козерог" , "Водолей", "Рыбы", "Овен", "Телец", "Близнецы", "Рак", "Лев", "Дева", "Весы", "Скорпион", "Стрелец"];
    let chinaSigns = ["Крысы", "Быка", "Тигра", "Кролика", "Дракона", "Змеи", "Лошади", "Козы", "Обезьяны", "Петуха", "Собаки", "Свиньи"];
    switch(dateZodiac) {
        case 0: {//January
            if(dayZodiac < 20)
                zodSigns = zodSigns[0];
            else
                zodSigns = zodSigns[1];
        }break;
        case 1: {//February
            if(dayZodiac < 19)
                zodSigns = zodSigns[1];
            else
                zodSigns = zodSigns[2];
        }break;
        case 2: {//March
            if(dayZodiac < 21)
                zodSigns = zodSigns[2];
            else
                zodSigns = zodSigns[3];
        }break;
        case 3: {//April
            if(dayZodiac < 20)
                zodSigns = zodSigns[3];
            else
                zodSigns = zodSigns[4];
        }break;
        case 4: {//May
            if(dayZodiac < 21)
                zodSigns = zodSigns[4];
            else
                zodSigns = zodSigns[5];
        }break;
        case 5: {//June
            if(dayZodiac < 21)
                zodSigns = zodSigns[5];
            else
                zodSigns = zodSigns[6];
        }break;
        case 6: {//July
            if(dayZodiac < 23)
                zodSigns = zodSigns[6];
            else
                zodSigns = zodSigns[7];
        }break;
        case 7: {//August
            if(dayZodiac < 23)
                zodSigns = zodSigns[7];
            else
                zodSigns = zodSigns[8];
        }break;
        case 8: {//September
            if(dayZodiac < 23)
                zodSigns = zodSigns[8];
            else
                zodSigns = zodSigns[9];
        }break;
        case 9: {//October
            if(dayZodiac < 23)
                zodSigns = zodSigns[9];
            else
                zodSigns = zodSigns[10];
        }break;
        case 10: {//November
            if(dayZodiac < 22)
                zodSigns = zodSigns[10];
            else
                zodSigns = zodSigns[11];
        }break;
        case 11: {//December
            if(dayZodiac < 22)
                zodSigns = zodSigns[11];
            else
                zodSigns = zodSigns[0];
        }break;
    }

    switch ((date.getFullYear() - 4) % 12) {
        case 0:
        {chinaSigns = chinaSigns[0]} break;

        case 1:
        {chinaSigns = chinaSigns[1]} break;
        case 2:
        {chinaSigns = chinaSigns[2]} break;
        case 3:
        {chinaSigns = chinaSigns[3]} break;
        case 4:
        {chinaSigns = chinaSigns[4]} break;
        case 5:
        {chinaSigns = chinaSigns[5]} break;

        case 6:
        {chinaSigns = chinaSigns[6]} break;

        case 7:
        {chinaSigns = chinaSigns[7]} break;

        case 8:
        {chinaSigns = chinaSigns[8]} break;

        case 9:
        {chinaSigns = chinaSigns[9]} break;

        case 10:
        {chinaSigns = chinaSigns[10]} break;

        case 11:
        {chinaSigns = chinaSigns[11]} break;

    }

    let result = function () {
        alert("Вам " + years + " лет!");
        alert("Ваш знак зодиака: " + zodSigns);
        alert("Вы родились в год " + chinaSigns);
    };
    return result();

}

console.log(zodiac());
